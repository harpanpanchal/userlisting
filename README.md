This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


## Important points

1) Initial data will be pulled from localStorage. If there is no data then it would be stored from initialState variable.
2) Search filter functions based on 'Firstname' 'Lastname' and 'Birthdate'. So, user needs to select one of these from the Filter dropdown.
3) Add: To add a new user, user needs to click 'Add New' button which would create new item in the store with blank data. User will then need to click on 'Edit' icon located in the last row within the table. Once the data is filled up for all the fields, user will need to click on 'Save' button located in the same row.
4) Delete: Click on 'Delete' icon to delete the user entry.