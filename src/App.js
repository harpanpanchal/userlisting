import React, { Component } from 'react';
import "./App.css";
import UserList from './components/UserList.jsx';
import SearchFilter from './components/SearchFilter';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {addUser,deleteUser,updateUser} from './actions/userActions.js'

class App extends Component {
  constructor(props)
  {
    super(props);
    this.addNewUser = this.addNewUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.editUserSubmit = this.editUserSubmit.bind(this);
    this.filterList = this.filterList.bind(this);
  };
  state = {
    selectionError: "Please Select Filter Option",
    searchTextError: "Please Enter Search Text"
  }
componentWillMount() {
    this.setState({ items: [...this.props.userList] })
  };

componentWillReceiveProps(newProps) {    
  this.setState({ items: [...newProps.userList] })
}

    filterList = (event) => {
      this.setState({ items: [...this.props.userList] });
      if (event.target.value === "") {
      this.setState({ items: [...this.props.userList] })
    }
    else {
        var updatedList = [...this.props.userList];
        const filterselection = document.querySelector(".filterselection").value;
        const invalidfilter = document.querySelector(".invalidfilter");
        const invalidsearch = document.querySelector(".invalidsearch");
        const searchText = document.querySelector(".searchText");
        if (filterselection === "select") {
          invalidfilter.innerHTML = this.state.selectionError;
        }
        else if (searchText.value === "") {
          invalidsearch.innerHTML = this.state.searchTextError;
          invalidsearch.innerHtml = "";
        }
        else {
          invalidsearch.innerHtml = "";
          invalidfilter.innerHTML = "";
          updatedList = updatedList.filter(function (item) {            
          return item[filterselection].toLowerCase().search(event.target.value.toLowerCase()) !== -1;
           });
           this.setState({ items: updatedList });         
        }      
    }
};  

addNewUser()
  {
    this.props.addUser({ id: Math.max(...this.props.userList.map(function (o) { return o.id })) + 1, firstname: '', lastname: '', birthdate: '' });
  }
  deleteUser(id)
  {
      this.props.deleteUser(id);
  }
  editUserSubmit(id,firstname,lastname,birthdate)
  {
    if (firstname !== "" || lastname !== "" || birthdate !== "") {
    this.props.updateUser({ id: id, firstname: firstname, lastname: lastname, birthdate: birthdate });
    this.setState({ items: [...this.props.userList] });
}
  }

render() {
return (
      <div className="container-fluid">
      <div className="row mt-3"><div className="col-lg-12">
      <div className="card">
      <div className="card-header"><h1>User List</h1>
  </div>
        <div className="card-body">
          <SearchFilter filterList={this.filterList} />
          
<table className="table table-hover">
          <thead className="thead-dark"><tr><th>First Name</th><th>Last Name</th><th>Birthdate</th><th>Edit/Save</th><th>Delete</th></tr></thead>
          <UserList deleteUser={this.deleteUser} userList={this.state.items} editUserSubmit={this.editUserSubmit}/>
        </table>
        <button className="btn btn-dark pull-left" onClick={this.addNewUser}>Add New</button>
      </div></div></div></div></div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userList : state
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addUser:addUser,
    deleteUser:deleteUser,
    updateUser:updateUser
  },dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(App);