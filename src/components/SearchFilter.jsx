import React from 'react';

const SearchFilter = (props) => {
    return (<div className="filter-list">
        <div className="container-fluid text-center">
            <div className="row">
                <div className="col-2 mb-3 offset-3">
                    <label htmlFor="country">Filter</label>
                    <select className="form-control form-control text-center filterselection" id="filterselection" required="Please select Filter">
                        <option value="select">Please select</option>
                        <option value="firstname">First Name</option>
                        <option value="lastname">Last Name</option>
                        <option value="birthdate">Birthdate</option>
                    </select>
                    <div className="invalidfilter">
                    </div>
                </div>
                <div className="col-4 mb-3">
                    <label htmlFor="state">Search</label>
                    <input type="text" className="form-control form-control searchText" aria-label="Search" placeholder="Search" onChange={props.filterList} />
                    <div className="invalidsearch">
                    </div>
                </div>

            </div>
        </div>
    </div>)
};
export default SearchFilter;