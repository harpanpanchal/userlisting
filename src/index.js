import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';
import {createStore} from 'redux';
import userReducer from './reducers/userReducer';
import {Provider} from 'react-redux'

let initialState = [{id:1,firstname:'John',lastname:"Doe",birthdate:'12/12/2000'},{id:2,firstname:'Steve',lastname:'Smith',birthdate:'12/12/1979'}
,{id:3,firstname:'Riya',lastname:"Sen",birthdate:'15/12/1978'},{id:4,firstname:'Roy',lastname:"Kapoor",birthdate:'27/11/2004'}];

if( localStorage.getItem("users") === null)
localStorage.setItem('users',JSON.stringify(initialState));
else 
initialState = JSON.parse(localStorage.getItem('users'));
const store = createStore(userReducer,initialState);

ReactDOM.render(
<Provider store={store}><App /></Provider>, document.getElementById('root'));