import React, { Component } from 'react';
export default class UserItem extends Component {
  constructor(props) {
    super(props);
    this.state = { isEdit: false }
    this.editUser = this.editUser.bind(this);
    this.editUserSubmit = this.editUserSubmit.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
  }
  deleteUser() {
    const { id } = this.props.user;
    this.props.deleteUser(id);

  }
  editUser() {
    this.setState((prevState, props) => ({
      isEdit: !prevState.isEdit
    }))
  }
  editUserSubmit() {
    this.setState((prevState, props) => ({
      isEdit: !prevState.isEdit
    }));
    this.props.editUserSubmit(this.props.user.id, this.firstnameInput.value, this.lastnameInput.value, this.birthdateInput.value);
  }
  render() {
    const { firstname, lastname, birthdate } = this.props.user;
    return (
      this.state.isEdit === true ?
        <tr className="bg-warning" key={this.props.index}><td><input ref={firstnameInput => this.firstnameInput = firstnameInput} defaultValue={firstname} /></td><td><input defaultValue={lastname} ref={lastnameInput => this.lastnameInput = lastnameInput} /></td><td><input ref={birthdateInput => this.birthdateInput = birthdateInput} defaultValue={birthdate} /></td><td><i className="far fa-save" onClick={this.editUserSubmit}></i></td><td><i className="fas fa-trash"></i></td></tr>
        :
        <tr key={this.props.index}><td>{firstname}</td><td>{lastname}</td><td>{birthdate}</td><td><i className="far fa-edit" onClick={this.editUser}></i></td><td><i className="fas fa-trash" onClick={this.deleteUser}></i></td></tr>
    );
  }
}